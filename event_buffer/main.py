import tango
import time
import numpy as np
import matplotlib
import matplotlib.pyplot as plt
import warnings

from .receiver import EventReceiver
from .buffer import Buffer

warnings.simplefilter("ignore", UserWarning)
matplotlib.use('Qt5Agg')

def main():
    # Create an object buffer and an event object
    buffer = Buffer(5)
    event = EventReceiver(listener=buffer.append)

    # Async subscribe to the attribute device
    event.connect('bl06/vc/ipct-01', 'i1')
    input()


if __name__ == '__main__':
    main()
