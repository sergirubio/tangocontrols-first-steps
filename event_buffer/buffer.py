import numpy as np
import matplotlib
import matplotlib.pyplot as plt
import warnings

class MyPlot:
    def __init__(self, buffer=None):
        self.buffer = buffer

    def update_plot(self, buffer):
        self.buffer = buffer
        plt.plot(self.buffer)
        plt.ion()
        plt.draw()
        plt.pause(1)
        plt.clf()
        
class Buffer:
    def __init__(self, size):
        """
        creates a buffer with the size that we have passed to it, like a queue structure
        """
        self.plot = None
        self.buffer = []
        self.size = size

    def append(self, value):
        self.buffer.append(value)
        # self.buffer = self.buffer[-self.size:]
        while len(self.buffer) > self.size:
            self.buffer.pop(0)
        self.refresh_plot()
        return self.buffer

    def refresh_plot(self):
        self.plot = MyPlot(np.expm1(self.buffer))
        self.plot.update_plot(np.expm1(self.buffer))
        print(np.expm1(self.buffer))
