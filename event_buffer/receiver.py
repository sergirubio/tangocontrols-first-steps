import tango
import time


class EventReceiver:
    def __init__(self, listener=None):
        """
        listener: if we pass a callable function, it will be executed for each value received
        if listener is not specified, the value will be simply stored as last_value
        """
        self.proxy = None
        self.last_event = None
        self.last_event_time = 0
        self.last_value = None
        self.event_id = None
        self.listener = listener

    def connect(self, device, attribute):
        self.proxy = tango.DeviceProxy(device)
        self.event_id = self.proxy.subscribe_event(attribute, tango.EventType.CHANGE_EVENT, self.callback)

    def disconnect(self):
        self.proxy.unsubscribe_event(self.event_id)
        self.event_id = None

    def __del__(self):
        if self.event_id is not None:
            self.disconnect()

    def callback(self, event):
        self.last_event = event
        self.last_event_time = time.time()
        try:
            self.last_value = event.attr_value.value
            if self.listener:
                self.listener(self.last_value)
        except Exception as e:
            self.last_value = e
